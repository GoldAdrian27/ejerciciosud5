import java.util.Scanner;

public class Ejercicio10App {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Cuantos productos tienes?");
		int productos = teclado.nextInt();
		double suma = 0;
		for (int i = 1; i <= productos; i++) {
			
			System.out.println("Introduce el precio del producto " + i);
			suma += teclado.nextDouble();
		}
		System.out.println("Precio total: " + suma);
	}

}
