import java.util.Scanner;


public class Ejercicio13App {

	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce el primer numero");
		int num1 = teclado.nextInt();
		System.out.println("Introduce el segundo numero");
		int num2 = teclado.nextInt();
		System.out.println("Introduce la operacion que quieres realizar");
		String simbolo = teclado.next();
		
		
		switch (simbolo) {
		case "+":
			System.out.println(num1+num2);
			break;
		case "-":
			System.out.println(num1-num2);
			break;
		case "/":
			System.out.println(num1/num2);
			break;
		case "*":
			System.out.println(num1*num2);
			break;
		case "%":
			System.out.println(num1%num2);
			break;
		case "^":
			System.out.println(Math.pow(num1, num2));
			break;

		default:
			System.out.println("simbolo no reconocido");
			break;
		}
	}

}
