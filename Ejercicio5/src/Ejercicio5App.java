import java.util.Scanner;

public class Ejercicio5App {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Introduce un numero");
		
		int num = teclado.nextInt();
		if(num%2 == 0) {
			System.out.println("Es divisible entre 2");
		}else {
			System.out.println("No es divisible entre 2");

		}
	}

}
